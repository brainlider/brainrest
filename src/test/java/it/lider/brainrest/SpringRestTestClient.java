package it.lider.brainrest;


//import it.lider.brainrest.entity.Cliente;
import it.lider.brainrest.controller.*;

import org.springframework.web.client.RestTemplate;

public class SpringRestTestClient {
	
	public static final String REST_SERVICE_URI = "http://localhost:8080/brainrest";
	
	
	
	
	/* GET */
    private static void getCliente(){
        System.out.println("Testing getUser API----------");
        RestTemplate restTemplate = new RestTemplate();
        ClienteMetadata cliente = restTemplate.getForObject(REST_SERVICE_URI+"/cliente/ce004", ClienteMetadata.class);
       
        System.out.println(cliente.getData().getCod_cliente());
        System.out.println(cliente.getForeign_key());
        System.out.println(cliente.getPrimary_key());
    }

    public static void main(String args[]) throws Exception{
    	
    	getCliente();
    }
}
