package it.lider.brainrest.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.lider.brainrest.entity.Cliente;
import it.lider.brainrest.service.ServiceDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;



@RestController
public class RestControllerBrain {
	


	@Autowired
	private ServiceDao serviceDao;

//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/cliente/{cod_cliente}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Map<String,List<Map<String,Object>>>> getCliente(@PathVariable("cod_cliente") String cod_cliente) throws Exception {
//
//		System.out.println("restituisci cliente per codice cliente " + cod_cliente);
//		Cliente cliente = serviceDao.findCliente(cod_cliente);
//		if (cliente == null) {
//			System.out.println("cliente " + cod_cliente + " non trovato");
//			return new ResponseEntity<Map<String,List<Map<String,Object>>>>(HttpStatus.NOT_FOUND);
//		}
//		Map<String,Object> map2 = new HashMap<String,Object>();
//		map2.put("PRIMARY_KEY", "cod_cliente");
//		List<Map<String,Object>> list4 = new ArrayList<Map<String,Object>>();
//		list4.add(map2);
//		Map<String,Object> map = serviceDao.toMap(cliente);
//		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
//		list.add(map);
//		List<Map<String,String>> list2 = serviceDao.getTableInformation("cliente");
//		List<Map<String,Object>> list3 = new ArrayList<Map<String,Object>>();
//
//		list3.addAll((Collection<? extends Map<String, Object>>) list2);
//		Map<String,List<Map<String,Object>>> mp = new HashMap<String,List<Map<String,Object>>>();
//		mp.put("PRIMARY_KEY", list4);
//		mp.put("metadata", list3);
//		mp.put("data",list);
//		
//
//		return new ResponseEntity<Map<String,List<Map<String,Object>>>>(mp, HttpStatus.OK);
//	}
//	
	
	
	@RequestMapping(value = "/cliente/{cod_cliente}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ClienteMetadata> getCliente(@PathVariable("cod_cliente") String cod_cliente) throws Exception {

		System.out.println("restituisci cliente per codice cliente " + cod_cliente);
		Cliente cliente = serviceDao.findCliente(cod_cliente);
		ClienteMetadata clienteMetadata = new ClienteMetadata();
		clienteMetadata.setData(cliente);
		clienteMetadata.setMetadata(serviceDao.getTableInformation("cliente"));
		clienteMetadata.setPrimary_key(serviceDao.getTableInformationPK("cliente"));
		clienteMetadata.setForeign_key(serviceDao.getTableInformationFK("cliente"));
		if (cliente == null) {
			System.out.println("cliente " + cod_cliente + " non trovato");
			return new ResponseEntity<ClienteMetadata>(HttpStatus.NOT_FOUND);
		}
		
		
		return new ResponseEntity<ClienteMetadata>(clienteMetadata, HttpStatus.OK);
	}

	@RequestMapping(value = "/cliente/", method = RequestMethod.POST)
	public ResponseEntity<Void> createCliente(@RequestBody Cliente cliente, UriComponentsBuilder ucBuilder) throws Exception {
		System.out.println("creazione cliente " + cliente.getCod_cliente());

		if (serviceDao.verificaId(cliente)) {
			System.out.println("Cliente con codice " + cliente.getCod_cliente() + " esiste già");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		serviceDao.addCliente(cliente);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/cliente/{cod_cliente}").buildAndExpand(cliente.getCod_cliente()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	
	@RequestMapping(value="/tables", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ArrayList<String>> getTables() throws Exception {
		ArrayList<String> list=null;
		list = serviceDao.getTablesMetadata();

			
		return new ResponseEntity<ArrayList<String>>(list, HttpStatus.OK);
	}
	
	@RequestMapping(value="/tables/{nomeTabella}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Map<String, String>>> getTablesSchema(@PathVariable("nomeTabella") String nomeTabella) throws Exception {
		List<Map<String, String>> map=null;
		
		map = serviceDao.getTableInformation(nomeTabella);
		

		
		return new ResponseEntity<List<Map<String, String>>>(map, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/cliente/{cod_cliente}", method = RequestMethod.DELETE)
    public ResponseEntity<Cliente> deleteCliente(@PathVariable("cod_cliente") String cod_cliente) throws Exception {
        System.out.println("cerca e cancellazione cliente con codice " + cod_cliente);
 
        Cliente cliente = serviceDao.findCliente(cod_cliente);
        if (cliente == null) {
            System.out.println("Impossibile cancellare codice " + cod_cliente + " non trovato");
            return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
        }
 
        serviceDao.delete(cod_cliente);
        return new ResponseEntity<Cliente>(HttpStatus.NO_CONTENT);
    }
	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/cliente/", method = RequestMethod.GET)
//    public ResponseEntity<Map<String,List<Map<String,Object>>>> listClienti() throws Exception {
//        List<Cliente> listCliente = serviceDao.getListCliente();
//        if(listCliente.isEmpty()){
//            return new ResponseEntity<Map<String,List<Map<String,Object>>>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//        }
//        List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
//        
//        for (Cliente c : listCliente){
//        	
//        	Map<String,Object> mp = new HashMap<String,Object>();
//        	mp = serviceDao.toMap(c);
//        	listMap.add(mp);
//        	
//        }
//        Map<String,Object> map2 = new HashMap<String,Object>();
//		map2.put("PRIMARY_KEY", "cod_cliente");
//		List<Map<String,Object>> list4 = new ArrayList<Map<String,Object>>();
//		list4.add(map2);
//        List<Map<String,String>> list2 = serviceDao.getTableInformation("cliente");
//        List<Map<String,Object>> list3 = new ArrayList<Map<String,Object>>();
//        list3.addAll((Collection<? extends Map<String, Object>>) list2);
//
//        Map<String,List<Map<String,Object>>> map = new HashMap<String,List<Map<String,Object>>>();
//        map.put("PRIMARY_KEY", list4);
//        map.put("data",listMap);
//        map.put("metadata", list3);
//        return new ResponseEntity<Map<String,List<Map<String,Object>>>>(map, HttpStatus.OK);
//    }
	
	@RequestMapping(value = "/cliente/", method = RequestMethod.GET)
    public ResponseEntity<ListClienteMetadata> listClienti() throws Exception {
        List<Cliente> list = serviceDao.getListCliente();
        ListClienteMetadata listClienteMetadata = new ListClienteMetadata();
        listClienteMetadata.setData(list);
        listClienteMetadata.setMetadata(serviceDao.getTableInformation("cliente"));
        listClienteMetadata.setPrimary_key(serviceDao.getTableInformationPK("cliente"));
        listClienteMetadata.setForeign_key(serviceDao.getTableInformationFK("cliente"));
        if(list.isEmpty()){
            return new ResponseEntity<ListClienteMetadata>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<ListClienteMetadata>(listClienteMetadata, HttpStatus.OK);
    }
	
	
	@RequestMapping(value = "/cliente/{cod_cliente}", method = RequestMethod.PUT)
    public ResponseEntity<Cliente> updateUser(@PathVariable("cod_cliente") String cod_cliente, @RequestBody Cliente cliente) throws Exception {
        System.out.println("Updating Cliente " + cod_cliente);
         
        Cliente currentCliente = serviceDao.findCliente(cod_cliente);
         
        if (currentCliente==null) {
            System.out.println("Cliente con codice " + cod_cliente + " non trovato");
            return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
        }
        currentCliente.setId(cliente.getId());
        currentCliente.setArea(cliente.getArea());
        currentCliente.setNome_cliente(cliente.getNome_cliente());
        currentCliente.setNum_punti_vendita(cliente.getNum_punti_vendita());
        currentCliente.setParco_installato_bilance(cliente.getParco_installato_bilance());
        currentCliente.setModelli_bilancia(cliente.getModelli_bilancia());
        currentCliente.setParco_installato_software(cliente.getParco_installato_software());
        currentCliente.setInfrastruttura_it(cliente.getInfrastruttura_it());
        currentCliente.setSistemi_operativi(cliente.getSistemi_operativi());
        currentCliente.setServer_presenti(cliente.getServer_presenti());
        currentCliente.setDatabase_presenti(cliente.getDatabase_presenti());
        
        
        serviceDao.update(currentCliente); 
        
        return new ResponseEntity<Cliente>(currentCliente, HttpStatus.OK);
    }
}
