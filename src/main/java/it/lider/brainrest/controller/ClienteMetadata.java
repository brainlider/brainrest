package it.lider.brainrest.controller;

import java.util.List;
import java.util.Map;

import it.lider.brainrest.entity.Cliente;

public class ClienteMetadata {
	
	private Cliente data;
	private List<Map<String, String>> metadata;
	private List<String> primary_key;
	private List<Map<String, String>> foreign_key;

	
	public List<Map<String, String>> getMetadata() {
		return metadata;
	}
	public void setMetadata(List<Map<String, String>> metadata) {
		this.metadata = metadata;
	}
	public List<String> getPrimary_key() {
		return primary_key;
	}
	public void setPrimary_key(List<String> primary_key) {
		this.primary_key = primary_key;
	}
	
	
	
	
	public Cliente getData() {
		return data;
	}
	public void setData(Cliente data) {
		this.data = data;
	}
	public List<Map<String, String>> getForeign_key() {
		return foreign_key;
	}
	public void setForeign_key(List<Map<String, String>> foreign_key) {
		this.foreign_key = foreign_key;
	}
	
	
	

}
