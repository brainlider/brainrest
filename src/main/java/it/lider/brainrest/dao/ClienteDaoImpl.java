package it.lider.brainrest.dao;

import it.lider.brainrest.entity.Cliente;












import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.ArrayList;
import java.util.List;










import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



//@Service("clienteDao")
//@Transactional
@Repository
public class ClienteDaoImpl implements ClienteDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private  DatabaseMetaData metadata = null;
	private  Connection connection = null;
	
	public ClienteDaoImpl() {
		
	}

	// Metodo per l'iniezione della dipendenza della SessionFactory:
	public ClienteDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        System.out.println("Ho iniettato la SessionFactory: " + sessionFactory);
	}
	
	@Transactional(readOnly = false)
	public void addCliente(Cliente cliente) throws Exception {
		
		System.out.println("inserisci cliente "+cliente.toString());
    	sessionFactory.getCurrentSession().save(cliente);
		
		
	}
	@Transactional
	public Cliente findCliente(String cod_cliente) throws Exception {
		System.out.println("inserisci codice cliente per trovate utente");
    	Cliente cliente=(Cliente)sessionFactory.getCurrentSession().get(Cliente.class, cod_cliente);
		
		return cliente;
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cliente> getListCliente() throws Exception{
		List<Cliente> listCliente=null;
		
		 Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Cliente.class);
         listCliente = criteria.list(); 
		
		return listCliente;
	}
	
	@Transactional
	public void delete(String cod_cliente) throws Exception{
		System.out.println("cancella cliente");
		Cliente clienteCanc = findCliente(cod_cliente);
		sessionFactory.getCurrentSession().delete(clienteCanc);

	}

	@Transactional
	public void update(Cliente cliente) throws Exception{
		sessionFactory.getCurrentSession().update(cliente);
		
		}
	
	@Transactional
	public boolean verificaId(Cliente cliente) throws Exception {
		if (findCliente(cliente.getCod_cliente())!= null){
			return true;
		}
		else{
			return false;
		}
	}

	public ArrayList<String> getTablesMetadata() throws SQLException {
		connection = jdbcTemplate.getDataSource().getConnection();
		metadata = connection.getMetaData();
		String table[] = { "TABLE" };
		 
        ResultSet rs = null;

        ArrayList<String> tables = null;

        // receive the Type of the object in a String array.

        rs = metadata.getTables(null, null, null, table);

        tables = new ArrayList<String>();

        while (rs.next()) {

            tables.add(rs.getString("TABLE_NAME"));

        }
        connection.close();
        return tables;
		
	}

	public List<Map<String, String>> getTableInformation(String nome)
			throws SQLException {
		connection = jdbcTemplate.getDataSource().getConnection();
		metadata = connection.getMetaData();
		ResultSet rs = null;
//		ResultSet rs2 = null;
		 try {
			rs = metadata.getColumns(null, null,nome,null);
//			rs2 = metadata.getPrimaryKeys(null, null, nome);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		 List<Map<String, String>> list = new ArrayList<Map<String, String>>();
//		 List<Map<String, String>> list2 = new ArrayList<Map<String, String>>();
//		 List<Map<String, String>> list3 = new ArrayList<Map<String, String>>();
		 
		 ResultSetMetaData meta=rs.getMetaData();
//		 ResultSetMetaData meta2=rs2.getMetaData();
		
		 while (rs.next()) {

		Map<String,String> mp = new HashMap<String, String>();
           for (int i = 1; i <= meta.getColumnCount(); i++) {
               String key = meta.getColumnName(i);
               String value = rs.getString(key);

               mp.put(key, value);

           }
	            list.add(mp);
	        }
//		 while (rs2.next()) {
//
//				Map<String,String> map = new HashMap<String, String>();
//		           for (int i = 1; i <= meta2.getColumnCount(); i++) {
//		               String key = meta2.getColumnName(i);
//		               String value = rs2.getString(key);
//
//		               map.put(key, value);
//
//		           }
//			            list2.add(map);
//			        }
//		 list3.addAll(list);
//		 list3.addAll(list2);
		 connection.close();
		 return list;
	 }

	@Override
	public List<String> getTableInformationPK(String nome)
			throws SQLException {
		connection = jdbcTemplate.getDataSource().getConnection();
		metadata = connection.getMetaData();
		ResultSet rs = null;
		try {
			
			rs = metadata.getPrimaryKeys(null, null, nome);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		 List<String> list = new ArrayList<String>();
		 ResultSetMetaData meta=rs.getMetaData();

		
		 while (rs.next()) {

			
		
           for (int i = 1; i <= meta.getColumnCount(); i++) {
               String key = meta.getColumnName(i);
               String value = rs.getString(key);

               if("column_name".equalsIgnoreCase(key))
               {
                      		
	            list.add(value);
               }
           	}
		 }
		connection.close();
		return list;
	}

	@Override
	public List<Map<String, String>> getTableInformationFK(String nome)
			throws SQLException {
		connection = jdbcTemplate.getDataSource().getConnection();
		metadata = connection.getMetaData();
		ResultSet rs = null;
		try {
			
			rs = metadata.getExportedKeys(null, null, nome);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		 List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		 ResultSetMetaData meta=rs.getMetaData();

		
		 while (rs.next()) {

		Map<String,String> mp = new HashMap<String, String>();
           for (int i = 1; i <= meta.getColumnCount(); i++) {
               String key = meta.getColumnName(i);
               String value = rs.getString(key);

               mp.put(key, value);

           }
	            list.add(mp);
	        }
		
		return list;
	}

		
	}
	


