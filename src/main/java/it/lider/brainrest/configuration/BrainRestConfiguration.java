package it.lider.brainrest.configuration;

import it.lider.brainrest.dao.ClienteDao;
import it.lider.brainrest.dao.ClienteDaoImpl;
import it.lider.brainrest.entity.Cliente;
import it.lider.brainrest.service.ServiceDao;
import it.lider.brainrest.service.ServiceDaoImpl;



import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration
@EnableWebMvc
@ComponentScan("it.lider.brainrest")
@EnableTransactionManagement
//@PropertySource("classpath:config.properties")
public class BrainRestConfiguration {
	
//	@Autowired
//	private Environment env;
//	
	
	@Bean(name = "dataSource")
    public DataSource getDataSource() {
    	BasicDataSource dataSource = new BasicDataSource();
//    	dataSource.setDriverClassName(env.getProperty("MYSQL_DB_DRIVER_CLASS"));
//    	dataSource.setUrl(env.getProperty("MYSQL_DB_URL"));
//    	dataSource.setUsername(env.getProperty("MYSQL_DB_USERNAME"));
//    	dataSource.setPassword(env.getProperty("MYSQL_DB_PASSWORD"));
    	dataSource.setDriverClassName("com.mysql.jdbc.Driver");
    	dataSource.setUrl("jdbc:mysql://localhost:3306/brain2");
    	dataSource.setUsername("root");
    	dataSource.setPassword("paolo");
    	dataSource.setMaxTotal(100);
    	
    	return dataSource;
    }
	
	@Autowired
	@Bean(name = "jdbcTemplate")
	public JdbcTemplate getJdbcTemplate(DataSource ds){
		
		return new JdbcTemplate(ds);
	}
	
	 private Properties getHibernateProperties() {
	    	Properties properties = new Properties();
	    	
	    	properties.put("hibernate.show_sql", "true");
	    	properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	    	return properties;
	    }
	 
	 @Autowired
	    @Bean(name = "sessionFactory")
	    public SessionFactory getSessionFactory(DataSource dataSource) {
	    	LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
	    	sessionBuilder.addProperties(getHibernateProperties());
	    	sessionBuilder.addAnnotatedClasses(Cliente.class);
	    	return sessionBuilder.buildSessionFactory();
	    }

	 @Autowired
		@Bean(name = "transactionManager")
		public HibernateTransactionManager getTransactionManager(
				SessionFactory sessionFactory) {
			HibernateTransactionManager transactionManager = new HibernateTransactionManager(
					sessionFactory);

			return transactionManager;
		}
	 
	 @Autowired
	    @Bean(name = "clienteDao")
	    public ClienteDao getClienteDao(SessionFactory sessionFactory) {
	    	return new ClienteDaoImpl(sessionFactory);
	    }
	 
	 @Autowired
	    @Bean(name = "serviceDao")
	    public ServiceDao getServiceDao(ClienteDao clienteDao) {
	    	return new ServiceDaoImpl(clienteDao);
	    }
}
