package it.lider.brainrest.service;

import it.lider.brainrest.dao.ClienteDao;
import it.lider.brainrest.entity.Cliente;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;






import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ServiceDaoImpl implements ServiceDao {
	
	@Autowired
	ClienteDao clienteDao;
	
	public ServiceDaoImpl(){}
	// Metodo per l'iniezione della dipendenza :
		public ServiceDaoImpl(ClienteDao clienteDao) {
	        this.clienteDao = clienteDao;
	       
		}
		
	@Transactional
	public void addCliente(Cliente cliente) throws Exception {
		
		clienteDao.addCliente(cliente);
	}
	
	@Transactional
	public Cliente findCliente(String cod_cliente) throws Exception {
		
		return clienteDao.findCliente(cod_cliente);
	}
	
	@Transactional
	public List<Cliente> getListCliente() throws Exception {
		
		return clienteDao.getListCliente();
	}
	
	@Transactional
	public void delete(String cod_cliente) throws Exception {
		clienteDao.delete(cod_cliente);
	}
	
	@Transactional
	public void update(Cliente cliente) throws Exception {
		clienteDao.update(cliente);
		
	}
	
	@Transactional
	public boolean verificaId(Cliente cliente) throws Exception {
		return clienteDao.verificaId(cliente);
		
	}
	public ArrayList<String> getTablesMetadata() throws SQLException {
		
		return clienteDao.getTablesMetadata();
	}
	public List<Map<String, String>> getTableInformation(String nome)
			throws SQLException {
		
		return clienteDao.getTableInformation(nome);
	}
	@Override
	public Map<String, Object> toMap(Object object) throws Exception {
		Map<String,Object> mp = new HashMap<String, Object>();
	    for ( Field field : object.getClass().getDeclaredFields() )
	    {
	        field.setAccessible( true );
	        mp.put( field.getName(), field.get( object ) );
	    }
	    
	    return mp;
		
	}
	@Override
	public List< String> getTableInformationPK(String nome)
			throws SQLException {
		
		return clienteDao.getTableInformationPK(nome);
	}
	@Override
	public List<Map<String, String>> getTableInformationFK(String nome)
			throws SQLException {
		
		return clienteDao.getTableInformationFK(nome);
	}
	
//	@Override
//	public List<Map<String, Object>> toMap(Object object) throws Exception {
//		
//		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
//		Map<String,Object> mp = new HashMap<String, Object>();
//	    for ( Field field : object.getClass().getDeclaredFields() )
//	    {
//	        field.setAccessible( true );
//	        mp.put( field.getName(), field.get( object ) );
//	    }
//	    list.add(mp);
//	    return list;
//	}

}
