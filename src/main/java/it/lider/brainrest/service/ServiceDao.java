package it.lider.brainrest.service;


import it.lider.brainrest.entity.Cliente;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ServiceDao {
	public void addCliente(Cliente cliente)throws Exception;
	public Cliente findCliente(String cod_cliente)throws Exception;
	public List<Cliente> getListCliente()throws Exception;
	public void delete(String cod_cliente)throws Exception;
	public void update(Cliente cliente)throws Exception;
	public boolean verificaId(Cliente cliente)throws Exception;
	public  ArrayList<String> getTablesMetadata() throws SQLException;
	public List<Map<String, String>> getTableInformation(String nome) throws SQLException;
	public List<String> getTableInformationPK(String nome) throws SQLException;
	public List<Map<String, String>> getTableInformationFK(String nome) throws SQLException;
	public Map<String, Object> toMap(Object object) throws Exception;
}
