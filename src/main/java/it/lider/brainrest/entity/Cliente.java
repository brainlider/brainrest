package it.lider.brainrest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="cliente")
public class Cliente {
	
			
	@Column(name = "id")
	private Integer id;
	
	@Id
//    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "cod_cliente")
	private String cod_cliente;
	
	@Column(name = "area")
	private String area;
	
	@Column(name = "nome_cliente")
	private String nome_cliente;
	
	@Column(name = "num_punti_vendita")
	private Integer num_punti_vendita;
	
	@Column(name = "parco_installato_bilance")
	private Integer parco_installato_bilance;
	
	@Column(name = "modelli_bilancia")
	private String modelli_bilancia;
	
	@Column(name = "parco_installato_software")
	private String parco_installato_software;
	
	@Column(name = "infrastruttura_it")
	private String infrastruttura_it;
	
	@Column(name = "sistemi_operativi")
	private String sistemi_operativi;
	
	@Column(name = "server_presenti")
	private String server_presenti;
	
	@Column(name = "database_presenti")
	private String database_presenti;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCod_cliente() {
		return cod_cliente;
	}

	public void setCod_cliente(String cod_cliente) {
		this.cod_cliente = cod_cliente;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getNome_cliente() {
		return nome_cliente;
	}

	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}

	public Integer getNum_punti_vendita() {
		return num_punti_vendita;
	}

	public void setNum_punti_vendita(Integer num_punti_vendita) {
		this.num_punti_vendita = num_punti_vendita;
	}

	public Integer getParco_installato_bilance() {
		return parco_installato_bilance;
	}

	public void setParco_installato_bilance(Integer parco_installato_bilance) {
		this.parco_installato_bilance = parco_installato_bilance;
	}

	public String getModelli_bilancia() {
		return modelli_bilancia;
	}

	public void setModelli_bilancia(String modelli_bilancia) {
		this.modelli_bilancia = modelli_bilancia;
	}

	public String getParco_installato_software() {
		return parco_installato_software;
	}

	public void setParco_installato_software(String parco_installato_software) {
		this.parco_installato_software = parco_installato_software;
	}

	public String getInfrastruttura_it() {
		return infrastruttura_it;
	}

	public void setInfrastruttura_it(String infrastruttura_it) {
		this.infrastruttura_it = infrastruttura_it;
	}

	public String getSistemi_operativi() {
		return sistemi_operativi;
	}

	public void setSistemi_operativi(String sistemi_operativi) {
		this.sistemi_operativi = sistemi_operativi;
	}

	public String getServer_presenti() {
		return server_presenti;
	}

	public void setServer_presenti(String server_presenti) {
		this.server_presenti = server_presenti;
	}

	public String getDatabase_presenti() {
		return database_presenti;
	}

	public void setDatabase_presenti(String database_presenti) {
		this.database_presenti = database_presenti;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", cod_cliente=" + cod_cliente + ", area="
				+ area + ", nome_cliente=" + nome_cliente
				+ ", num_punti_vendita=" + num_punti_vendita
				+ ", parco_installato_bilance=" + parco_installato_bilance
				+ ", modelli_bilancia=" + modelli_bilancia
				+ ", parco_installato_software=" + parco_installato_software
				+ ", infrastruttura_it=" + infrastruttura_it
				+ ", sistemi_operativi=" + sistemi_operativi
				+ ", server_presenti=" + server_presenti
				+ ", database_presenti=" + database_presenti + "]";
	}
	
	

}
